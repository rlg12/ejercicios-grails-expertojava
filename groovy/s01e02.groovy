class Libro {
    /* Añade aquí la definición de la clase */
    String nombre
    Integer anyo
    String autor
    String editorial
    Libro(String nombre, String autor,Integer anyo){
        this.nombre =nombre
        this.anyo = anyo
        this.autor = autor
    }
    String getAutor(){
        def token = autor.tokenize(",")
        return token[1].trim()+" "+token[0].trim()
    }
}

/* Crea aquí las tres instancias de libro l1, l2 y l3 */
def l1 = new Libro("La colmena","Cela Trulock, Camilo José",1951)
def l2 = new Libro("La galatea","Cervantes Saavedra, Miguel",1585)
def l3 = new Libro("La colmena", "Lope de Vega y Carpio, Félix Arturo",1632)

assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() ==1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

/* Añade aquí la asignación de la editorial a todos los libros */
l1.setEditorial('Anaya')
l2.setEditorial('Planeta')
l3.setEditorial('Santillana')


assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'
