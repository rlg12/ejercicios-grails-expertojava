/* Añade aquí la implementación solicitada en el ejercicio */

Number.metaClass.moneda = {String locale ->
      delegate[beginIndex-1..delegate.size()-1]}

Number.metaClass.moneda = { String locale ->
      if(locale == "en_EN"){
        return "£"+delegate
      }else if(locale == "en_US"){
        return "\$"+delegate
      }else if(locale == "es_ES"){
        return delegate+"€"
      }else {
        return delegate
      }
}

assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"
