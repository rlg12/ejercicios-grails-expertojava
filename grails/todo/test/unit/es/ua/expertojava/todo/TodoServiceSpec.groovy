package es.ua.expertojava.todo

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "El método getNextTodos devuelve los siguientes todos de los días pasado por parámetro"() {
        given:
        def usuario1 = new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2')
        def todoDayBeforeYesterday = new Todo(title:"Todo day before yesterday", date: new Date() - 2,finalizado:false, user: usuario1 )
        def todoYesterday = new Todo(title:"Todo yesterday", date: new Date() - 1,finalizado:false , user: usuario1 )
        def todoToday = new Todo(title:"Todo today", date: new Date(),finalizado:false , user: usuario1 )
        def todoTomorrow = new Todo(title:"Todo tomorrow", date: new Date() + 1 ,finalizado:false , user: usuario1 )
        def todoDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 2 ,finalizado:false , user: usuario1 )
        def todoDayAfterDayAfterTomorrow = new Todo(title:"Todo day after tomorrow", date: new Date() + 3 ,finalizado:false , user: usuario1 )
        and:
        mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
        def nextTodos = service.getNextTodos(2,[:])
        expect:
        Todo.count() == 6
        and:
        nextTodos.containsAll([todoTomorrow, todoDayAfterTomorrow])
        nextTodos.size() == 2
        and:
        !nextTodos.contains(todoDayBeforeYesterday)
        !nextTodos.contains(todoToday)
        !nextTodos.contains(todoYesterday)
        !nextTodos.contains(todoDayAfterDayAfterTomorrow)
    }

    void "Test Metodo SaveTodo que compruebe el almacenamiento de campos correctamente"() {
        given:
        def usuario1 = new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2')
        def t1 = new Todo(title:"Todo day before yesterday", date: new Date() - 2,finalizado:true , user: usuario1 )
        def t2 = new Todo(title:"Todo yesterday", date: new Date() - 1,finalizado:false , user: usuario1 )
        def t3 = new Todo(title:"Todo today", date: new Date(),finalizado:false , user: usuario1 )
        and:
        mockDomain(Todo,[t1, t2, t3])
        and:
        def to1 = service.saveTodo(t1)
        def to2 = service.saveTodo(t2)
        def to3 = service.saveTodo(t3)

        expect:
        Todo.count() == 3
        and:
        Todo.list().containsAll([to1, to2,to3])
        to1.dateDone.format("dd/MM/yyyy") == new Date().format("dd/MM/yyyy")
        to2.dateDone == null
        to3.dateDone == null
    }
}
