package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    def "Si la fecha reminder es menor a la fecha de la tarea funciona correctamente"() {
        given:
        def t1 = new Todo(title: "un titulo", description: "Descripcion", reminderDate: new Date()-variable, finalizado: "false", date: new Date())
        when:
        t1.validate()
        then:
        !t1?.errors['reminderDate']
        where:
        variable << [1,2,3,4,5]
    }
    @Unroll
    def "Si la fecha reminder es mayor a la fecha de la tarea falla"() {
        given:
        def t2 = new Todo(title: "un titulo", description: "Descripcion", reminderDate: new Date()+variable, finalizado: "false", date: new Date())
        when:
        t2.validate()
        then:
        t2?.errors['reminderDate']
        where:
        variable << [1,2,3,4,5]
    }
}
