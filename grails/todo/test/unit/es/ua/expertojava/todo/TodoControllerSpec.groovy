package es.ua.expertojava.todo



import grails.test.mixin.*
import spock.lang.*

@TestFor(TodoController)
@Mock(Todo)
class TodoControllerSpec extends Specification {
    static transactional = false
    def todoService = new TodoService()

    def setup(){
        def usuario1 = new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2')
        controller.springSecurityService = [currentUser : usuario1]
        controller.todoService = todoService
    }

    def populateValidParams(params) {

        assert params != null
        def usuario1 = new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2')
        params["title"] = 'Title name'
        params["description"] = 'Description name'
        params["reminderDate"] = new Date()-3
        params["date"] = new Date()
        params["dateDone"] = new Date()
        params["finalizado"] = true
        params["user"] = usuario1
    }


    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.todoInstanceList
            model.todoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.todoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def todo = new Todo()
            todo.validate()
            controller.save(todo)

        then:"The create view is rendered again with the correct model"
            model.todoInstance!= null
            view == 'create'
        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params)

            controller.save(todo)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/todo/show/1'
            controller.flash.message != null
            Todo.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.show(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.edit(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/todo/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def todo = new Todo()
            todo.validate()
            controller.update(todo)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.todoInstance == todo

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params).save(flush: true)
            controller.update(todo)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/todo/show/$todo.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/todo/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def todo = new Todo(params).save(flush: true)

        then:"It exists"
            Todo.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(todo)

        then:"The instance is deleted"
            Todo.count() == 0
            response.redirectedUrl == '/todo/index'
            flash.message != null
    }

    def "Si cuantos elementos de la lista de tareas se terminan en 1,2,3,4,5 dias"() {
        given:
        def c1 = new Todo(title:"title1",description:"description", date: new Date()+1,finalizado:false,user: controller.springSecurityService.currentUser )
        def c2 = new Todo(title:"title1",description:"description", date: new Date()+2,finalizado:false ,user: controller.springSecurityService.currentUser )
        def c3 = new Todo(title:"title1",description:"description", date: new Date()+3,finalizado:false ,user: controller.springSecurityService.currentUser )
        def c4 = new Todo(title:"title1",description:"description", date: new Date()+4,finalizado:false ,user: controller.springSecurityService.currentUser )
        def c5 = new Todo(title:"title1",description:"description", date: new Date()+5,finalizado:false ,user: controller.springSecurityService.currentUser )

        when:
        c1.save(flush: true)
        c2.save(flush: true)
        c3.save(flush: true)
        c4.save(flush: true)
        c5.save(flush: true)

        controller.listNextTodos(valor)

        then:
        view == 'index'
        model.todoInstanceCount == esperado

        where:
        valor   |   esperado
        1       |   1
        2       |   2
        3       |   3
        4       |   4
        5       |   5

    }
    def "Listados Por categorias"() {
        given:
        def c1 = new Category(name:"Categoria1")
        def c2 = new Category(name:"Categoria2")
        def c3 = new Category(name:"Categoria3")


        when:

        c1.save(flush: true)
        c2.save(flush: true)
        c3.save(flush: true)

        controller.listByCategory()

        then:"Se devuelven todas las categorias creadas"

        model.categoryInstanceList.size() == 3

    }
}
