package es.ua.expertojava.todo

class Todo {
    String title
    String description
    Date date
    Date reminderDate
    String url
    Category category
    Boolean finalizado
    Date dateCreated
    Date lastUpdated
    Date dateDone
    User user

    static searchable = [except: ['date', 'reminderDate']]

    static hasMany = [tags:Tag]
    static belongsTo = [Tag]

    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        reminderDate(nullable:true,
                validator: { val, obj ->
                    if (val && obj.date) {
                        return val.before(obj?.date)
                    }
                    return true
                }
        )
        url(nullable:true, url:true)
        category(nullable:true)
        finalizado(blank: false)
        dateDone(nullable: true)
    }

    String toString(){
        "title"
    }

}