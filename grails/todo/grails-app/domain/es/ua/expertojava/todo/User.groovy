package es.ua.expertojava.todo

/**
 * Created by Rafa on 14/02/16.
 */
class User extends Person{
    String name
    String surnames
    String confirmPassword
    String email
    Date dateOfBirth
    String description

    static searchable = true

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false)
        surnames(blank:false)
        confirmPassword(blank:false, password:true)
        email(blank:false, email:true)
        dateOfBirth(nullable:true, validator: {
            if (it?.compareTo(new Date()) < 0)
                return true
            return false
        })
        description(maxSize:1000,nullable:true)
    }

    static transients = ["confirmPassword"]

    String toString(){
        "@${username}"
    }

    String getTipo(){
        String tipo = ""
        getAuthorities().each {
            if(it.authority =="ROLE_ADMIN"){
                tipo = "ROLE_ADMIN"
            }
        }
       if(tipo.isEmpty()){
           tipo = "ROLE_BASIC"
       }

        return tipo
    }
}