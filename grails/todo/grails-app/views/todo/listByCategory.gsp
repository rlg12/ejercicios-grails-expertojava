<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>
<div id="list-todo" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>


    <table>
        <thead>
        <tr>

            <g:sortableColumn property="name" title="${message(code: 'category.name.label', default: 'Name')}" />




        </tr>
        </thead>
        <tbody>
        <g:form url="[resource:todoInstance, action:'listByCategoryResult']" >

        <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">


                <td><g:checkBox name="seleccionados" value="${fieldValue(bean: categoryInstance, field: "id")}"/>
                ${fieldValue(bean: categoryInstance, field: "name")}
                </td>

                <%--	<td>${fieldValue(bean: categoryInstance, field: "description")}</td>--%>

            </tr>
        </g:each>
            <fieldset class="buttons">
                <g:actionSubmit class="button" action="listByCategoryResult" value="LISTADO"/>

            </fieldset>
        </g:form>
        </tbody>

    </table>



</div>
</body>
</html>