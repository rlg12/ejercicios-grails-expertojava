package es.ua.expertojava.todo

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
@Secured(['ROLE_BASIC'])
@Transactional(readOnly = true)
class TodoController {
    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def todoService
    @Secured(['ROLE_BASIC'])
    def index(Integer max) {
        def user = springSecurityService.currentUser
        params.max = Math.min(max ?: 10, 100)

        respond Todo.findAllByUser(user,params), model:[todoInstanceCount: Todo.countByUser(user,params)]
    }

    def listByCategory() {

        respond Category.list()
    }
    @Secured(['ROLE_ADMIN'])
    def showTodosByUser(){
      //  def user = springSecurityService.currentUser
        def user = User.findByUsername(params.username)
       respond Todo.findAllByUser(user)
    }

    def busquedaHorasPrevio() {
        def user = springSecurityService.currentUser
        respond  Todo.findAllByUser(user), model:[todoInstanceCount: Todo.countByUser(user)]
    }

    def lastTodosDone(Integer horas) {
        respond todoService.lastTodosDone(horas)
    }
    def listByCategoryResult(params) {
        def selectedCategories = params.seleccionados.collect{it.toLong()}

         respond todoService.listByCategory(Category.getAll(selectedCategories))
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        def user = springSecurityService.currentUser
     //   def todo = new Todo(params)
        params.user = user
      //  todo.user=user
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {
        def user = springSecurityService.currentUser
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        todoInstance.user=user
        //todoInstance.save flush:true
        todoInstance= todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

      //  todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        //todoInstance.delete flush:true
        todoService.removeTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
                model:[todoInstanceCount:
                               todoService.countNextTodos(days)],
                view:"index"
    }
}
