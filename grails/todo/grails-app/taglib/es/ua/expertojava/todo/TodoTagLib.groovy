package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = 'todo'

    def printIconFromBoolean  = { valor ->
        if(valor.value) {
            out << asset.image(src:"ok.jpg", width : "20px", height:"20px")
            //out << asset.image(src:"ok.jpg")
        } else {
            out << asset.image(src:"ko.png", width : "20px", height:"20px")
            //out << asset.image(src:"ko.png")
        }
    }
}
