package es.ua.expertojava.todo

class TodoService {
  //  def springSecurityService

    def removeTodo(Todo instancia) {
        def tags = instancia.tags

        tags.each {temp ->
            temp.todos.remove(instancia)
        }
        instancia.delete()


    }

    def listByCategory(List<Category> lista) {

        return Todo.findAllByCategoryInList(lista,["sort":"date","order":"asc"])
    }


    def saveTodo(Todo todoInstance){
        //def user = springSecurityService.currentUser
        if(todoInstance.finalizado){
            todoInstance.setDateDone(new Date())
        }
      //  todoInstance.user = user
        todoInstance.save flush:true
        return todoInstance
    }

    def lastTodosDone(Integer horas){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, (-1)*horas);

        Todo.findAllByDateDoneBetween(calendar.getTime(),new Date())
    }
    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }
    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }
}
