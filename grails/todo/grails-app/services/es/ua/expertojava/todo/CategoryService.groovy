package es.ua.expertojava.todo

import grails.transaction.Transactional

//@Transactional
class CategoryService {

    def removeCategory(Category instancia) {

        instancia?.todos?.each {todo ->
            todo.category = null
        }
        instancia.delete()

    }
}
