package todo

import org.apache.log4j.Logger

class LogFilters {
    def log = Logger.getLogger("grails.app.conf.todo.LogFilters")
    def springSecurityService
    def filters = {
        all(controller:'todo|category|tag|user', action:'create|edit|index|show') {
            before = {

            }
            after = { Map model ->
                        getControllerName()
                        log.trace(" User " + springSecurityService.getCurrentUser() + " - Controlador " + controllerName + " - Accion " + getActionName() +" - Modelo "+ model)
            }
            afterView = { Exception e ->

            }
        }
    }
}
