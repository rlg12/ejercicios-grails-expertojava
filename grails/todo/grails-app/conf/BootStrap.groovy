import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        // USuarios Seguridad

        def Admin = new User(username: "admin",password:"admin",name:"admin",surnames:"Administrador",confirmPassword: "admin",email:'adminUno@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario Administrador').save()
        def User1 = new User(username: "usuario1",password:"usuario1",name:"usuario1",surnames:"Usuario 1",confirmPassword: "usuario1",email:'usu1@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 1').save()
        def User2= new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2').save()

        def rolAdmin = new Role(authority:"ROLE_ADMIN").save()
        def rolUSER =  new Role(authority:"ROLE_BASIC").save()

        PersonRole.create Admin, rolAdmin, true
        PersonRole.create User1, rolUSER, true
        PersonRole.create User2, rolUSER, true

        def categoryHome = new Category(name:"Hogar").save()
        def categoryJob = new Category(name:"Trabajo").save()

        def tagEasy = new Tag(name:"Fácil", color:"#312478").save()
        def tagDifficult = new Tag(name:"Difícil", color:"#0055FF").save()
        def tagArt = new Tag(name:"Arte",color:"#0055FF").save()
        def tagRoutine = new Tag(name:"Rutina",color:"#0052FF").save()
        def tagKitchen = new Tag(name:"Cocina",color:"#0055AA").save()

        def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1,finalizado:false,user: User1)
        def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2,finalizado:false,user: User2)
        def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4,finalizado:false,user: User1)
        def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(),finalizado:true, user: User1)


        todoPaintKitchen.addToTags(tagDifficult)
        todoPaintKitchen.addToTags(tagArt)
        todoPaintKitchen.addToTags(tagKitchen)
        todoPaintKitchen.category = categoryHome
        todoPaintKitchen.save()

        todoCollectPost.addToTags(tagRoutine)
        todoCollectPost.category = categoryJob
        todoCollectPost.save()

        todoBakeCake.addToTags(tagEasy)
        todoBakeCake.addToTags(tagKitchen)
        todoBakeCake.category = categoryHome
        todoBakeCake.save()

        todoWriteUnitTests.addToTags(tagEasy)
        todoWriteUnitTests.category = categoryJob
        todoWriteUnitTests.save()
    }
    def destroy = { }
}
